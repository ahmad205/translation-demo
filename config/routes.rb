require 'api_version'
Rails.application.routes.draw do
  resources :translations
  resources :translations_ens
  resources :translations_ars
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiVersion.new('v1', true) do
      resources :translations
    end
  end

  # root to: 'pages#index'
  get "pages" => "pages#index"
  get "change_locale" => "pages#change_locale"

end

