json.extract! translations_en, :id, :text, :translation, :created_at, :updated_at
json.url translations_en_url(translations_en, format: :json)
