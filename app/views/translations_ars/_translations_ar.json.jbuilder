json.extract! translations_ar, :id, :text, :translation, :created_at, :updated_at
json.url translations_ar_url(translations_ar, format: :json)
