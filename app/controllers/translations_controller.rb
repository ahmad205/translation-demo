class TranslationsController < ApplicationController
  before_action :set_translation, only: [:show, :edit, :update, :destroy]

  # GET /translations
  # GET /translations.json
  def index
    @translations = Translation.all
    respond_to do |format|
      format.html
      format.json { render json: @translations }
    end
  end

  # GET /translations/1
  # GET /translations/1.json
  def show
  end

  # GET /translations/new
  def new
    @translation = Translation.new
  end

  # GET /translations/1/edit
  def edit
  end

  # POST /translations
  # POST /translations.json
  def create
    @translation = Translation.new(translation_params)

    respond_to do |format|
      if @translation.save
        @text = params[:translation][:text].to_s
        @controller = params[:translation][:controller].to_s

        @data_en = YAML.load_file "config/locales/en/#{@controller}.yml"
        @data_ar = YAML.load_file "config/locales/ar/#{@controller}.yml"
        @data_en["en"]["#{@controller}"]["#{@text}"] = params[:translation][:english_translation].to_s
        @data_ar["ar"]["#{@controller}"]["#{@text}"] = params[:translation][:arabic_translation].to_s
        File.open("config/locales/en/#{@controller}.yml", 'w') { |f| YAML.dump(@data_en, f) }
        File.open("config/locales/ar/#{@controller}.yml", 'w') { |f| YAML.dump(@data_ar, f) }

        format.html { redirect_to @translation, notice: 'Translation was successfully created.' }
        format.json { render :show, status: :created, location: @translation }
      else
        format.html { render :new }
        format.json { render json: @translation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translations/1
  # PATCH/PUT /translations/1.json
  def update
    respond_to do |format|
      if @translation.update(translation_params)

        @text = params[:translation][:text].to_s
        @controller = params[:translation][:controller].to_s
        @data_en = YAML.load_file "config/locales/en/#{@controller}.yml"
        @data_ar = YAML.load_file "config/locales/ar/#{@controller}.yml"
        @data_en["en"]["#{@controller}"]["#{@text}"] = params[:translation][:english_translation].to_s
        @data_ar["ar"]["#{@controller}"]["#{@text}"] = params[:translation][:arabic_translation].to_s
        File.open("config/locales/en/#{@controller}.yml", 'w') { |f| YAML.dump(@data_en, f) }
        File.open("config/locales/ar/#{@controller}.yml", 'w') { |f| YAML.dump(@data_ar, f) }

        format.html { redirect_to @translation, notice: 'Translation was successfully updated.' }
        format.json { render :show, status: :ok, location: @translation }
      else
        format.html { render :edit }
        format.json { render json: @translation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translations/1
  # DELETE /translations/1.json
  def destroy
    @translation.destroy
    respond_to do |format|
      format.html { redirect_to translations_url, notice: 'Translation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translation
      @translation = Translation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translation_params
      params.require(:translation).permit(:text, :arabic_translation, :english_translation , :controller)
    end
end
