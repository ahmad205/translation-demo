class TranslationsEnsController < ApplicationController
  before_action :set_translations_en, only: [:show, :edit, :update, :destroy]

  # GET /translations_ens
  # GET /translations_ens.json
  def index
    @translations_ens = TranslationsEn.all
  end

  # GET /translations_ens/1
  # GET /translations_ens/1.json
  def show
  end

  # GET /translations_ens/new
  def new
    @translations_en = TranslationsEn.new
  end

  # GET /translations_ens/1/edit
  def edit
  end

  # POST /translations_ens
  # POST /translations_ens.json
  def create
    @translations_en = TranslationsEn.new(translations_en_params)

    respond_to do |format|
      if @translations_en.save
        format.html { redirect_to @translations_en, notice: 'Translations en was successfully created.' }
        format.json { render :show, status: :created, location: @translations_en }
      else
        format.html { render :new }
        format.json { render json: @translations_en.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translations_ens/1
  # PATCH/PUT /translations_ens/1.json
  def update
    respond_to do |format|
      if @translations_en.update(translations_en_params)

        @textt = params[:translations_en][:text].to_s
        @data = YAML.load_file "config/locales/en.yml"
        @data["en"]["#{@textt}"] = params[:translations_en][:translation]
        File.open("config/locales/en.yml", 'w') { |f| YAML.dump(@data, f) }

        format.html { redirect_to @translations_en, notice: 'Translations en was successfully updated.' }
        format.json { render :show, status: :ok, location: @translations_en }
      else
        format.html { render :edit }
        format.json { render json: @translations_en.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translations_ens/1
  # DELETE /translations_ens/1.json
  def destroy
    @translations_en.destroy
    respond_to do |format|
      format.html { redirect_to translations_ens_url, notice: 'Translations en was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translations_en
      @translations_en = TranslationsEn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translations_en_params
      params.require(:translations_en).permit(:text, :translation)
    end
end
