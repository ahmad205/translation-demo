class TranslationsArsController < ApplicationController
  before_action :set_translations_ar, only: [:show, :edit, :update, :destroy]

  # GET /translations_ars
  # GET /translations_ars.json
  def index
    @translations_ars = TranslationsAr.all
  end

  # GET /translations_ars/1
  # GET /translations_ars/1.json
  def show
  end

  # GET /translations_ars/new
  def new
    @translations_ar = TranslationsAr.new
  end

  # GET /translations_ars/1/edit
  def edit
  end

  # POST /translations_ars
  # POST /translations_ars.json
  def create
    @translations_ar = TranslationsAr.new(translations_ar_params)

    respond_to do |format|
      if @translations_ar.save
        format.html { redirect_to @translations_ar, notice: 'Translations ar was successfully created.' }
        format.json { render :show, status: :created, location: @translations_ar }
      else
        format.html { render :new }
        format.json { render json: @translations_ar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translations_ars/1
  # PATCH/PUT /translations_ars/1.json
  def update
    respond_to do |format|
      if @translations_ar.update(translations_ar_params)
        @textt = params[:translations_ar][:text].to_s
        @data = YAML.load_file "config/locales/ar.yml"
        @data["ar"]["#{@textt}"] = params[:translations_ar][:translation]
        File.open("config/locales/ar.yml", 'w') { |f| YAML.dump(@data, f) }

        format.html { redirect_to @translations_ar, notice: 'Translations ar was successfully updated.' }
        format.json { render :show, status: :ok, location: @translations_ar }
      else
        format.html { render :edit }
        format.json { render json: @translations_ar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translations_ars/1
  # DELETE /translations_ars/1.json
  def destroy
    @translations_ar.destroy
    respond_to do |format|
      format.html { redirect_to translations_ars_url, notice: 'Translations ar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translations_ar
      @translations_ar = TranslationsAr.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translations_ar_params
      params.require(:translations_ar).permit(:text, :translation)
    end
end
