class PagesController < ApplicationController
    def index
      @data_en = YAML.load_file "config/locales/ar/user.yml"
    end

    def change_locale
        I18n.locale = params[:locale] 
        redirect_to(request.env['HTTP_REFERER']) 
    end

  end