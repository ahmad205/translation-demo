module Api
  module V1
    class TranslationsController < ApplicationController
      before_action :set_translation, only: [:show, :edit, :update, :destroy]
      #respond_to :json
      # GET /translations
      # GET /translations.json
      def index

      # =begin
      # @api {get} /api/translations 1-Request translations List
      # @apiVersion 0.3.0
      # @apiName GetTranslation
      # @apiGroup Translations
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/translations
      # @apiSuccess {Number} id translation unique ID.
      # @apiSuccess {String} text original text.
      # @apiSuccess {String} arabic_translation arabic translation for text.
      # @apiSuccess {String} english_translation english translation for text.
      # @apiSuccess {Date} created_at  Date created.
      # @apiSuccess {Date} updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # [
      #     {
      #         "id": 1,
      #         "text": "payers",
      #         "arabic_translation": "بايرز",
      #         "english_translation": "payers",
      #         "created_at": "2018-07-19T11:02:30.110Z",
      #         "updated_at": "2018-07-19T11:02:30.110Z",             
      #     },
      #     {
      #         "id": 2,
      #         "text": "welcome",
      #         "arabic_translation": "مرحبا",
      #         "english_translation": "welcome",
      #         "created_at": "2018-07-19T11:02:37.000Z",
      #         "updated_at": "2018-07-19T11:02:37.000Z",             
      #     }
      # ]
      # @apiError MissingToken invalid Token.
      # @apiErrorExample Error-Response:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end

        @translations = Translation.all
        respond_to do |format|
          format.json { render json: @translations }
        end
      end

      # GET /translations/1
      # GET /translations/1.json
      def show
        # =begin
        # @api {get} /api/translations/{:id} 3-Request Specific translation
        # @apiVersion 0.3.0
        # @apiName GetSpecificTranslation
        # @apiGroup Translations
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/translations/5
        # @apiParam {Number} id translation ID.
        # @apiSuccess {String} text original text.
        # @apiSuccess {String} arabic_translation arabic translation for text.
        # @apiSuccess {String} english_translation english translation for text.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 5,
        #         "text": "test5",
        #         "arabic_translation": "اختبار",
        #         "english_translation": "test",
        #         "created_at": "2018-07-19T11:02:37.000Z",
        #         "updated_at": "2018-07-19T11:02:37.000Z",             
        #     }
        # @apiError TranslationNotFound The id of the Translation was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TranslationNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
          format.json { render json: Translation.find(params[:id]) }
        end

      end

      # GET /translations/new
      def new
        @translation = Translation.new
      end

      # GET /translations/1/edit
      def edit
      end

      # POST /translations
      # POST /translations.json
      def create
        # =begin
        # @api {post} /api/translations 2-Create a new translation
        # @apiVersion 0.3.0
        # @apiName PostTranslation
        # @apiGroup Translations
        # @apiParam {String} text original text.
        # @apiParam {String} arabic_translation arabic translation for text.
        # @apiParam {String} english_translation english translation for text.
        # @apiSuccess {Number} id new translation ID.
        # @apiSuccess {String} text original text.
        # @apiSuccess {String} arabic_translation arabic translation for text.
        # @apiSuccess {String} english_translation english translation for text.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 4,
        #         "text": "ahmad",
        #         "arabic_translation": "احمد حسن",
        #         "english_translation": "ahmad hassan",
        #         "created_at": "2018-07-19T15:09:07.000Z",
        #         "updated_at": "2018-07-19T15:09:07.000Z",             
        #     }
        # @apiError TranslationNotFound The id of the Translation was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TranslationNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        # respond_to do |format|
        #   format.json { render json: Translation.create(translation_params) }
        # end

        respond_to do |format|
          @translation = Translation.new(translation_params)

            @text = params[:translation][:text].to_s
            @data_en = YAML.load_file "config/locales/en.yml"
            @data_ar = YAML.load_file "config/locales/ar.yml"
            @data_en["en"]["#{@text}"] = params[:translation][:english_translation].to_s
            @data_ar["ar"]["#{@text}"] = params[:translation][:arabic_translation].to_s
            File.open("config/locales/en.yml", 'w') { |f| YAML.dump(@data_en, f) }
            File.open("config/locales/ar.yml", 'w') { |f| YAML.dump(@data_ar, f) }

            format.json { render json: @translation }

        end
      end

      # PATCH/PUT /translations/1
      # PATCH/PUT /translations/1.json
      def update

        # =begin
        # @api {put} /api/translations/{:id} 4-Change an existing translation
        # @apiVersion 0.3.0
        # @apiName PutTranslation
        # @apiGroup Translations
        # @apiParam {Number} id translation ID.
        # @apiParam {String} arabic_translation arabic translation for text.
        # @apiParam {String} english_translation english translation for text.
        # @apiSuccess {String} text original text.
        # @apiSuccess {String} arabic_translation arabic translation for text.
        # @apiSuccess {String} english_translation english translation for text.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 4,
        #         "text": "ahmad",
        #         "arabic_translation": "احمد",
        #         "english_translation": "ahmad",
        #         "created_at": "2018-07-19T15:09:07.000Z",
        #         "updated_at": "2018-07-19T15:21:31.000Z",             
        #     }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        # respond_to do |format|
        #   format.json { render json: Translation.update(translation_params) }
        # end

        #respond_with Translation.update(translation_params)

        respond_to do |format|
          @translation.update(translation_params)

            @text = params[:translation][:text].to_s
            @data_en = YAML.load_file "config/locales/en.yml"
            @data_ar = YAML.load_file "config/locales/ar.yml"
            @data_en["en"]["#{@text}"] = params[:translation][:english_translation].to_s
            @data_ar["ar"]["#{@text}"] = params[:translation][:arabic_translation].to_s
            File.open("config/locales/en.yml", 'w') { |f| YAML.dump(@data_en, f) }
            File.open("config/locales/ar.yml", 'w') { |f| YAML.dump(@data_ar, f) }

            format.json { render json: Translation.update(translation_params) }

        end

      end

      # DELETE /translations/1
      # DELETE /translations/1.json
      def destroy
        
        # @translation.destroy
        # respond_to do |format|
        #   format.html { redirect_to translations_url, notice: 'Translation was successfully destroyed.' }
        #   format.json { head :no_content }
        # end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_translation
          @translation = Translation.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def translation_params
          params.require(:translation).permit(:text, :arabic_translation, :english_translation)
        end

    end

  end

end
