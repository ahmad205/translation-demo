define({ "api": [
  {
    "type": "get",
    "url": "/api/translations/{:id}",
    "title": "3-Request Specific translation",
    "version": "0.3.0",
    "name": "GetSpecificTranslation",
    "group": "Translations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/translations/5",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 5,\n        \"text\": \"test5\",\n        \"arabic_translation\": \"اختبار\",\n        \"english_translation\": \"test\",\n        \"created_at\": \"2018-07-19T11:02:37.000Z\",\n        \"updated_at\": \"2018-07-19T11:02:37.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TranslationNotFound",
            "description": "<p>The id of the Translation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TranslationNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "translation-demo/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "get",
    "url": "/api/translations",
    "title": "1-Request translations List",
    "version": "0.3.0",
    "name": "GetTranslation",
    "group": "Translations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/translations",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n    {\n        \"id\": 1,\n        \"text\": \"payers\",\n        \"arabic_translation\": \"بايرز\",\n        \"english_translation\": \"payers\",\n        \"created_at\": \"2018-07-19T11:02:30.110Z\",\n        \"updated_at\": \"2018-07-19T11:02:30.110Z\",             \n    },\n    {\n        \"id\": 2,\n        \"text\": \"welcome\",\n        \"arabic_translation\": \"مرحبا\",\n        \"english_translation\": \"welcome\",\n        \"created_at\": \"2018-07-19T11:02:37.000Z\",\n        \"updated_at\": \"2018-07-19T11:02:37.000Z\",             \n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "translation-demo/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "post",
    "url": "/api/translations",
    "title": "2-Create a new translation",
    "version": "0.3.0",
    "name": "PostTranslation",
    "group": "Translations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>new translation ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"text\": \"ahmad\",\n        \"arabic_translation\": \"احمد حسن\",\n        \"english_translation\": \"ahmad hassan\",\n        \"created_at\": \"2018-07-19T15:09:07.000Z\",\n        \"updated_at\": \"2018-07-19T15:09:07.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TranslationNotFound",
            "description": "<p>The id of the Translation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TranslationNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "translation-demo/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  },
  {
    "type": "put",
    "url": "/api/translations/{:id}",
    "title": "4-Change an existing translation",
    "version": "0.3.0",
    "name": "PutTranslation",
    "group": "Translations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>translation ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>original text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "arabic_translation",
            "description": "<p>arabic translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "english_translation",
            "description": "<p>english translation for text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"text\": \"ahmad\",\n        \"arabic_translation\": \"احمد\",\n        \"english_translation\": \"ahmad\",\n        \"created_at\": \"2018-07-19T15:09:07.000Z\",\n        \"updated_at\": \"2018-07-19T15:21:31.000Z\",             \n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "translation-demo/app/controllers/api/v1/translations_controller.rb",
    "groupTitle": "Translations"
  }
] });
