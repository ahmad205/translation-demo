define({
  "name": "example",
  "version": "0.3.0",
  "description": "apiDoc basic example",
  "title": "Custom apiDoc browser title",
  "url": "https://localhost:3000",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-07-19T13:21:01.863Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
