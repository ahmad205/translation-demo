class ApiVersion
  attr_reader :version, :default

  def initialize(version, default = false)
    @version = version
    @default = default
  end

  # check whether version is specified or is default
  def matches?(req)
    default || req.headers['Accept'].include?("application/vnd.translations.#{version}")
  end

end
