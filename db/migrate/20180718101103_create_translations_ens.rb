class CreateTranslationsEns < ActiveRecord::Migration[5.2]
  def change
    create_table :translations_ens do |t|
      t.string :text
      t.string :translation

      t.timestamps
    end
  end
end
