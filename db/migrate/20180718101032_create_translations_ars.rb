class CreateTranslationsArs < ActiveRecord::Migration[5.2]
  def change
    create_table :translations_ars do |t|
      t.string :text
      t.string :translation

      t.timestamps
    end
  end
end
