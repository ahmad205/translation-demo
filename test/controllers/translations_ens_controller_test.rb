require 'test_helper'

class TranslationsEnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @translations_en = translations_ens(:one)
  end

  test "should get index" do
    get translations_ens_url
    assert_response :success
  end

  test "should get new" do
    get new_translations_en_url
    assert_response :success
  end

  test "should create translations_en" do
    assert_difference('TranslationsEn.count') do
      post translations_ens_url, params: { translations_en: { text: @translations_en.text, translation: @translations_en.translation } }
    end

    assert_redirected_to translations_en_url(TranslationsEn.last)
  end

  test "should show translations_en" do
    get translations_en_url(@translations_en)
    assert_response :success
  end

  test "should get edit" do
    get edit_translations_en_url(@translations_en)
    assert_response :success
  end

  test "should update translations_en" do
    patch translations_en_url(@translations_en), params: { translations_en: { text: @translations_en.text, translation: @translations_en.translation } }
    assert_redirected_to translations_en_url(@translations_en)
  end

  test "should destroy translations_en" do
    assert_difference('TranslationsEn.count', -1) do
      delete translations_en_url(@translations_en)
    end

    assert_redirected_to translations_ens_url
  end
end
