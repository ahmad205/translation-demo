require 'test_helper'

class TranslationsArsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @translations_ar = translations_ars(:one)
  end

  test "should get index" do
    get translations_ars_url
    assert_response :success
  end

  test "should get new" do
    get new_translations_ar_url
    assert_response :success
  end

  test "should create translations_ar" do
    assert_difference('TranslationsAr.count') do
      post translations_ars_url, params: { translations_ar: { text: @translations_ar.text, translation: @translations_ar.translation } }
    end

    assert_redirected_to translations_ar_url(TranslationsAr.last)
  end

  test "should show translations_ar" do
    get translations_ar_url(@translations_ar)
    assert_response :success
  end

  test "should get edit" do
    get edit_translations_ar_url(@translations_ar)
    assert_response :success
  end

  test "should update translations_ar" do
    patch translations_ar_url(@translations_ar), params: { translations_ar: { text: @translations_ar.text, translation: @translations_ar.translation } }
    assert_redirected_to translations_ar_url(@translations_ar)
  end

  test "should destroy translations_ar" do
    assert_difference('TranslationsAr.count', -1) do
      delete translations_ar_url(@translations_ar)
    end

    assert_redirected_to translations_ars_url
  end
end
