require "application_system_test_case"

class TranslationsEnsTest < ApplicationSystemTestCase
  setup do
    @translations_en = translations_ens(:one)
  end

  test "visiting the index" do
    visit translations_ens_url
    assert_selector "h1", text: "Translations Ens"
  end

  test "creating a Translations en" do
    visit translations_ens_url
    click_on "New Translations En"

    fill_in "Text", with: @translations_en.text
    fill_in "Translation", with: @translations_en.translation
    click_on "Create Translations en"

    assert_text "Translations en was successfully created"
    click_on "Back"
  end

  test "updating a Translations en" do
    visit translations_ens_url
    click_on "Edit", match: :first

    fill_in "Text", with: @translations_en.text
    fill_in "Translation", with: @translations_en.translation
    click_on "Update Translations en"

    assert_text "Translations en was successfully updated"
    click_on "Back"
  end

  test "destroying a Translations en" do
    visit translations_ens_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Translations en was successfully destroyed"
  end
end
