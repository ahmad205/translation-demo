require "application_system_test_case"

class TranslationsArsTest < ApplicationSystemTestCase
  setup do
    @translations_ar = translations_ars(:one)
  end

  test "visiting the index" do
    visit translations_ars_url
    assert_selector "h1", text: "Translations Ars"
  end

  test "creating a Translations ar" do
    visit translations_ars_url
    click_on "New Translations Ar"

    fill_in "Text", with: @translations_ar.text
    fill_in "Translation", with: @translations_ar.translation
    click_on "Create Translations ar"

    assert_text "Translations ar was successfully created"
    click_on "Back"
  end

  test "updating a Translations ar" do
    visit translations_ars_url
    click_on "Edit", match: :first

    fill_in "Text", with: @translations_ar.text
    fill_in "Translation", with: @translations_ar.translation
    click_on "Update Translations ar"

    assert_text "Translations ar was successfully updated"
    click_on "Back"
  end

  test "destroying a Translations ar" do
    visit translations_ars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Translations ar was successfully destroyed"
  end
end
